package com.bbva.cognitus.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bbva.cognitus.databinding.NotificationItemBinding
import com.bbva.cognitus.models.Notification
import com.bbva.cognitus.utils.AlertDialogCustom

class NotificationsAdapter (private val items: List<Notification>) : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

	private var context: Context? = null
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		val inflater = LayoutInflater.from(parent.context)
		val binding = NotificationItemBinding.inflate(inflater)
		return ViewHolder(binding)
	}
	override fun getItemCount(): Int = items.size

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
		holder.binding.setClickListener {
			when (it!!.id) {
				holder.binding.notificationItem.id -> {
					AlertDialogCustom.createAlert(context!!, items[position].title, items[position].description!!, true)
				}
			}
		}
	}

	inner class ViewHolder(val binding: NotificationItemBinding) : RecyclerView.ViewHolder(binding.root) {
		fun bind(notificationModel: Notification) {
			binding.element = notificationModel
			binding.executePendingBindings()
		}
	}
}