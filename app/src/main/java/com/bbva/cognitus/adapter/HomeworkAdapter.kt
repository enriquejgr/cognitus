package com.bbva.cognitus.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bbva.cognitus.HomeworkReportActivity
import com.bbva.cognitus.R
import com.bbva.cognitus.databinding.HomeworkElementBinding
import com.bbva.cognitus.models.Homework

class HomeworkAdapter (private val items: List<Homework>) : RecyclerView.Adapter<HomeworkAdapter.ViewHolder>() {

	private var context: Context? = null

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		val inflater = LayoutInflater.from(parent.context)
		val binding = HomeworkElementBinding.inflate(inflater)
		return ViewHolder(binding)
	}

	override fun getItemCount(): Int = items.size

	@RequiresApi(Build.VERSION_CODES.M)
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
		holder.binding.setClickListener {
			when (it!!.id) {
				holder.binding.homeworkElement.id -> {
					val intent = Intent(context, HomeworkReportActivity::class.java)
					intent.putExtra("area", items[position].area)
					intent.putExtra("title", items[position].title)
					intent.putExtra("time", items[position].time)
					intent.putExtra("description", items[position].description)
					intent.putExtra("id", items[position].id)
					intent.putExtra("note", items[position].note)
					intent.putExtra("type", items[position].type)
					intent.putExtra("status", items[position].status)
					context?.startActivity(intent)
				}
			}
		}
	}

	inner class ViewHolder(val binding: HomeworkElementBinding) : RecyclerView.ViewHolder(binding.root) {
		@RequiresApi(Build.VERSION_CODES.M)
		fun bind(homeworkModel: Homework) {
			binding.element = homeworkModel
			if(homeworkModel.type == "1") {
				binding.tvPriority.text = context?.getString(R.string.homework_urgent)
				binding.homeworkElement.background = context?.getDrawable(R.drawable.back_task1)
				binding.tvPriority.setTextColor(context?.getColor(R.color.red)!!)
			} else {
				binding.tvPriority.text = context?.getString(R.string.homework_normal)
				binding.homeworkElement.background = context?.getDrawable(R.drawable.back_task2)
				binding.tvPriority.setTextColor(context?.getColor(R.color.green)!!)
			}
			binding.executePendingBindings()
		}
	}
}