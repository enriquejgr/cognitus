package com.bbva.cognitus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bbva.cognitus.NewsDetailActivity
import com.bbva.cognitus.WebviewActivity
import com.bbva.cognitus.databinding.NewItemBinding
import com.bbva.cognitus.models.News
import com.bbva.cognitus.utils.Utils

class NewsAdapter (private val items: List<News>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

	private var context: Context? =null
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		context = parent.context
		val inflater = LayoutInflater.from(parent.context)
		val binding = NewItemBinding.inflate(inflater)
		return ViewHolder(binding)
	}
	override fun getItemCount(): Int = items.size

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.bind(items[position])
		holder.binding.setClickListener {
			when (it!!.id) {
				holder.binding.ivVerMas.id -> {
					val intent = Intent(context, WebviewActivity::class.java)
					intent.putExtra("urlImg", items[position].notUrl)
					context?.startActivity(intent)
				}
				holder.binding.tvTitulo.id -> {
					val intent = Intent(context, NewsDetailActivity::class.java)
					intent.putExtra("title", items[position].notTitulo)
					intent.putExtra("description", items[position].notDesc)
					intent.putExtra("image", items[position].notImg)
					intent.putExtra("url", items[position].notUrl)
					context?.startActivity(intent)
				}
			}
		}
		Utils.loadImage(context,holder.binding.ivFotoNot, items[position].notImg.toString(), 1)
	}
	inner class ViewHolder(val binding: NewItemBinding) : RecyclerView.ViewHolder(binding.root) {
		fun bind(noticiaModel: News) {
			binding.elemento = noticiaModel
			binding.executePendingBindings()
		}
	}
}