package com.bbva.cognitus.firebase

data class Notification (
    val title: String?,
    val message: String?,
    val data: MutableMap<String,String>?
)