package com.bbva.cognitus.firebase

import android.content.Context
import android.util.Log
import com.bbva.cognitus.R
import com.bbva.cognitus.utils.SharedPreference
import com.crashlytics.android.Crashlytics

class RegisterCrash(private val context: Context) {

    private val tag = context.getString(R.string.app_name)

    private lateinit var sharedPreference: SharedPreference

    fun registerCrash(key: String, value: String) {
        sharedPreference = SharedPreference(context)
        Crashlytics.log(Log.DEBUG, tag, "Sending crash")
        Crashlytics.setString(key, value)
        Crashlytics.setUserIdentifier(sharedPreference.getValueString("userId"))
        Crashlytics.setUserName(sharedPreference.getValueString("userName"))
        Crashlytics.getInstance().crash()
    }
}