package com.bbva.cognitus

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.cognitus.databinding.ActivityHomeworkReportBinding
import com.bbva.cognitus.models.SimpleAnswer
import com.bbva.cognitus.task.HomeworkReportTask
import com.bbva.cognitus.utils.AlertDialogCustom
import com.bbva.cognitus.utils.SharedPreference
import com.bbva.cognitus.webservices.CallbackObject
import com.bbva.cognitus.webservices.WebServiceActivity

class HomeworkReportActivity: WebServiceActivity(), Validator.ValidationListener,
	CallbackObject<SimpleAnswer> {

	private val binding by lazy {
		DataBindingUtil.setContentView<ActivityHomeworkReportBinding>(this, R.layout.activity_homework_report)
	}

	private val idHomework by lazy { intent.getStringExtra("id") }

	private lateinit var sharedPreference: SharedPreference

	private val homeworkReportTask by lazy {
		HomeworkReportTask(this, this)
	}

	override fun onCreate(savedInstanceState : Bundle?) {
		super.onCreate(savedInstanceState)
		initializeToolbar(binding.toolBar, getString(R.string.activity_tasks_title), 0, getString(R.string.activity_tasks_subtitle))
		sharedPreference = SharedPreference(this)
		binding.tvArea.text = intent.getStringExtra("area")
		binding.tvTitle.text = intent.getStringExtra("title")
		binding.tvDescription.text = intent.getStringExtra("description")
		binding.tvTime.text = intent.getStringExtra("time")

		val validator = Validator(binding).also {
			it.setValidationListener(this)
		}
		binding.setClickListener {
			when(it!!.id) {
				binding.btnSave.id -> {
					validator.toValidate()
				}
				binding.btnFinish.id -> {
					/* TODO("To implement") */
				}
			}
		}
	}

	override fun onValidationSuccess() {
		homeworkReportTask.startWS(binding.etReportedTime.text.toString(),
			binding.etNote.text.toString(), idHomework, sharedPreference.getValueString("userId")!!)
	}

	override fun onValidationError() {}

	override fun obtainedValue(valor : SimpleAnswer) {
		if(valor.description != null) {
			AlertDialogCustom.createAlert(this, getString(R.string.app_name), valor.description!!, true)
		}
	}
}