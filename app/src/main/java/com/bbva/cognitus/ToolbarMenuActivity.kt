package com.bbva.cognitus

import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bbva.cognitus.databinding.ToolbarMenuBinding
import com.bbva.cognitus.utils.SharedPreference

abstract class ToolbarMenuActivity : AppCompatActivity() {

    private lateinit var sharedPreference: SharedPreference

    fun initializeToolbar(toolbarBinding: ToolbarMenuBinding) {
        setSupportActionBar(toolbarBinding.toolbar)
        sharedPreference = SharedPreference(this)
        toolbarBinding.setClickListener {
            when (it!!.id) {
                toolbarBinding.btnLogout.id -> {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle(getString(R.string.app_name))
                    builder.setMessage(getString(R.string.logout_confirm))
                    builder.setNegativeButton(getString((R.string.logout_button_negative))) {dialog, _ ->
                        dialog.dismiss()
                    }
                    builder.setPositiveButton(getString(R.string.logout_button_positive)) { dialog, _ ->
                        sharedPreference.clearSharedPreference()
                        dialog.dismiss()
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        }
    }
}