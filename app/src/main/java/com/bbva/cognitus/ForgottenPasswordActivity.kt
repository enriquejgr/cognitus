package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.cognitus.databinding.ActivityForgottenPasswordBinding
import com.bbva.cognitus.models.SimpleAnswer
import com.bbva.cognitus.task.RecoveryTask
import com.bbva.cognitus.utils.Utils
import com.bbva.cognitus.webservices.CallbackObject
import com.bbva.cognitus.webservices.WebServiceActivity

class ForgottenPasswordActivity : WebServiceActivity(), Validator.ValidationListener, CallbackObject<SimpleAnswer> {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityForgottenPasswordBinding>(this, R.layout.activity_forgotten_password)
    }

    private val taskRecovery by lazy { RecoveryTask(this, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeToolbar(binding.toolBar, getString(R.string.activity_forgotten_password_title), 0)

        val validator = Validator(binding).also {
            it.setValidationListener(this)
        }

        binding.setClickListener {
            when(it!!.id) {
                binding.btnRecovery.id -> {
                    validator.toValidate()
                }
            }
        }
    }

    override fun onValidationError() {
        Toast.makeText(this, "Hasta aqui todo mal", Toast.LENGTH_LONG).show()
    }

    override fun onValidationSuccess() {
        taskRecovery.startWS(binding.etEmail.text.toString())
    }

    override fun obtainedValue(valor: SimpleAnswer) {
        if(valor.description != null) {
            val utils: Utils? = Utils(this)
            val dialog = utils!!.setDialog(getString(R.string.app_name), valor.description!!)
            dialog.setOnDismissListener {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
    }

}
