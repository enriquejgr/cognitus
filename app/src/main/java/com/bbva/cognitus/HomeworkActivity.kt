package com.bbva.cognitus

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bbva.cognitus.adapter.HomeworkAdapter
import com.bbva.cognitus.databinding.ActivityHomeworkBinding
import com.bbva.cognitus.models.Homework
import com.bbva.cognitus.task.HomeworkTask
import com.bbva.cognitus.utils.SharedPreference
import com.bbva.cognitus.webservices.CallbackObjectList
import com.bbva.cognitus.webservices.WebServiceActivity

class HomeworkActivity: WebServiceActivity(),
	CallbackObjectList<Homework>,
	View.OnClickListener {

	private  val binding by lazy {
		DataBindingUtil.setContentView<ActivityHomeworkBinding>(this, R.layout.activity_homework)
	}

	private val taskHomework by lazy {
		HomeworkTask(this, this)
	}
	private var adapterHomework: RecyclerView.Adapter<*>? = null
	private lateinit var sharedPreference: SharedPreference

	override fun onCreate(savedInstanceState : Bundle?) {
		super.onCreate(savedInstanceState)
		initializeToolbar(binding.toolBar, getString(R.string.activity_tasks_title), 0)
		sharedPreference = SharedPreference(this)
		taskHomework.startWS(sharedPreference.getValueString("userId")!!)
	}

	override fun showList(homeworkList: List<Homework>?) {
		if(homeworkList != null) {
			val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
			binding.rvHomework.layoutManager = layoutManager
			this.adapterHomework = HomeworkAdapter(homeworkList!!)
			binding.rvHomework.adapter = adapterHomework
		}
	}

	override fun withoutResult() {}

	override fun onClick(view: View?) {}
}