package com.bbva.cognitus.task

import android.content.Context
import com.bbva.cognitus.models.SimpleAnswer
import com.bbva.cognitus.webservices.ApiService
import com.bbva.cognitus.webservices.CallbackObject
import com.bbva.cognitus.webservices.RetrofitClient
import com.bbva.cognitus.webservices.WSBaseTask
import retrofit2.Call

class RecoveryTask (context: Context, callback: CallbackObject<SimpleAnswer>):
    WSBaseTask<SimpleAnswer, CallbackObject<SimpleAnswer>>(context,callback) {

    fun startWS(email: String) {
        super.startWS(email)
    }

    override fun setIdService(): String {
        return "105"
    }

    override fun createCalled(encryptedParameters: String): Call<SimpleAnswer> {
        return RetrofitClient.createService(ApiService::class.java)
            .getRecovery(encryptedParameters)
    }

    override fun validAnswer(response: SimpleAnswer?): Boolean {
        return response?.err == false
    }

    override fun processResponse(response: SimpleAnswer?) {
        val msg = response?.description
        if(response?.err == false) {
            callback.successfulResult()
            callback.obtainedValue(response)
        } else {
            callback.failedQuery("" + msg)
        }
    }
}