package com.bbva.cognitus.task

import android.content.Context
import com.bbva.cognitus.models.User
import com.bbva.cognitus.models.response.UserResponse
import com.bbva.cognitus.webservices.ApiService
import com.bbva.cognitus.webservices.CallbackObject
import com.bbva.cognitus.webservices.RetrofitClient
import com.bbva.cognitus.webservices.WSBaseTask
import retrofit2.Call

class LoginTask (context: Context, callback: CallbackObject<User>):
    WSBaseTask<UserResponse, CallbackObject<User>>(context, callback) {

    fun startWS(name: String, email: String){
        super.startWS(name, email)
    }

    override fun setIdService(): String {
        return "100"
    }

    override fun createCalled(encryptedParameters: String): Call<UserResponse> {
        return RetrofitClient.createService(ApiService::class.java)
            .getUser(encryptedParameters)
    }

    override fun validAnswer(response: UserResponse?): Boolean {
        return response?.err == false && response.usuario != null
    }

    override fun processResponse(response: UserResponse?) {
        val userPac = response?.usuario
        val msg= response?.description
        if(userPac != null) {
            callback.successfulResult()
            callback.obtainedValue(userPac)
        } else
            callback.failedQuery(""+msg)
    }
}