package com.bbva.cognitus.task

import android.content.Context
import com.bbva.cognitus.models.News
import com.bbva.cognitus.models.response.NewsResponse
import com.bbva.cognitus.webservices.ApiService
import com.bbva.cognitus.webservices.CallbackObjectList
import com.bbva.cognitus.webservices.ObjectListTask
import com.bbva.cognitus.webservices.RetrofitClient
import retrofit2.Call

class NewsTask (context: Context, callback: CallbackObjectList<News>):
	ObjectListTask<NewsResponse, News>(context, callback) {

	fun startWS(){
		super.startWS()
	}

	override fun setIdService(): String {
		return "110"
	}

	override fun createCalled(encryptedParameters: String): Call<NewsResponse> {
		return RetrofitClient.createService(ApiService::class.java)
			.getNews(encryptedParameters)
	}

	override fun validAnswer(response: NewsResponse?): Boolean {
		return response?.newsList != null
	}

	override fun processResponse(response: NewsResponse?) {
		callback.successfulResult()
		if(response?.newsList?.isNotEmpty()==true) {
			callback.showList(response.newsList)
		} else
			callback.withoutResult()
	}
}