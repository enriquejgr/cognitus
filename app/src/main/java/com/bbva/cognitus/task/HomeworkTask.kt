package com.bbva.cognitus.task

import android.content.Context
import com.bbva.cognitus.models.Homework
import com.bbva.cognitus.models.response.HomeworkResponse
import com.bbva.cognitus.webservices.ApiService
import com.bbva.cognitus.webservices.CallbackObjectList
import com.bbva.cognitus.webservices.ObjectListTask
import com.bbva.cognitus.webservices.RetrofitClient
import retrofit2.Call

class HomeworkTask (context: Context, callback: CallbackObjectList<Homework>):
	ObjectListTask<HomeworkResponse, Homework>(context, callback) {

	fun startWS(id: String){
		super.startWS(id)
	}

	override fun setIdService(): String {
		return "120"
	}

	override fun createCalled(encryptedParameters : String) : Call<HomeworkResponse> {
		return RetrofitClient.createService(ApiService::class.java)
			.getHomeworks(encryptedParameters)
	}

	override fun validAnswer(response : HomeworkResponse?) : Boolean {
		return response?.homeworkList != null
	}

	override fun processResponse(response : HomeworkResponse?) {
		callback.successfulResult()
		if(response?.homeworkList?.isNotEmpty() == true) {
			callback.showList(response.homeworkList)
		} else {
			callback.withoutResult()
		}
	}
}