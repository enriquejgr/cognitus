package com.bbva.cognitus.task

import android.content.Context
import com.bbva.cognitus.models.Notification
import com.bbva.cognitus.models.response.NotificationResponse
import com.bbva.cognitus.webservices.ApiService
import com.bbva.cognitus.webservices.CallbackObjectList
import com.bbva.cognitus.webservices.ObjectListTask
import com.bbva.cognitus.webservices.RetrofitClient
import retrofit2.Call

class NotificationsTask (context: Context, callback: CallbackObjectList<Notification>):
	ObjectListTask<NotificationResponse, Notification>(context, callback) {

	fun startWS(id: String){
		super.startWS(id)
	}

	override fun setIdService(): String {
		return "115"
	}

	override fun createCalled(encryptedParameters : String) : Call<NotificationResponse> {
		return RetrofitClient.createService(ApiService::class.java)
			.getNotifications(encryptedParameters)
	}

	override fun validAnswer(response : NotificationResponse?) : Boolean {
		return response?.notificationsList != null
	}

	override fun processResponse(response : NotificationResponse?) {
		callback.successfulResult()
		if(response?.notificationsList?.isNotEmpty() == true) {
			callback.showList(response.notificationsList)
		} else {
			callback.withoutResult()
		}
	}
}