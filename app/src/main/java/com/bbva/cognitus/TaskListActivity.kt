package com.bbva.cognitus

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bbva.cognitus.databinding.ActivityTaskListBinding

class TaskListActivity : ToolbarActivity() {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityTaskListBinding>(this, R.layout.activity_task_list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeToolbar(binding.toolBar, getString(R.string.activity_tasks_title), 0)
    }
}