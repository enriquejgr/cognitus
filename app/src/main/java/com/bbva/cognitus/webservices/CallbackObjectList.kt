package com.bbva.cognitus.webservices

interface CallbackObjectList <O> : CallbackBasic {
	fun showList(listado: List<O>?)
	fun withoutResult()
}