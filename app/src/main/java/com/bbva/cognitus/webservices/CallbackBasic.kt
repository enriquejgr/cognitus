package com.bbva.cognitus.webservices

interface CallbackBasic {
    fun chargingStarted()
    fun successfulResult()
    fun failedQuery(causa: String?)
}