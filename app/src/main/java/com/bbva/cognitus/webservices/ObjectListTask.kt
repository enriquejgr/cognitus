package com.bbva.cognitus.webservices

import android.content.Context

abstract class ObjectListTask <S, O>(context: Context, callback: CallbackObjectList<O>) :
	WSBaseTask<S, CallbackObjectList<O>>(context, callback)