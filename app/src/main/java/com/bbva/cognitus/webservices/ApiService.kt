package com.bbva.cognitus.webservices

import com.bbva.cognitus.models.SimpleAnswer
import com.bbva.cognitus.models.response.HomeworkResponse
import com.bbva.cognitus.models.response.NewsResponse
import com.bbva.cognitus.models.response.NotificationResponse
import com.bbva.cognitus.models.response.UserResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {
    @FormUrlEncoded
    @POST("./")
    fun getUser(@Field("palabra")params: String): Call<UserResponse>

    @FormUrlEncoded
    @POST("./")
    fun getRecovery(@Field("palabra")params: String): Call<SimpleAnswer>

    @FormUrlEncoded
    @POST("./")
    fun getNews(@Field("palabra")params: String): Call<NewsResponse>

    @FormUrlEncoded
    @POST("./")
    fun getNotifications(@Field("palabra")params: String): Call<NotificationResponse>

    @FormUrlEncoded
    @POST("./")
    fun getHomeworks(@Field("palabra")params: String): Call<HomeworkResponse>

    @FormUrlEncoded
    @POST("./")
    fun reportHomework(@Field("palabra")params: String): Call<SimpleAnswer>
}