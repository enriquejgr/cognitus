package com.bbva.cognitus.webservices

import com.bbva.cognitus.ToolbarActivity
import com.bbva.cognitus.utils.Utils
import com.bbva.cognitus.utils.WaitingDialog

abstract class WebServiceActivity : ToolbarActivity(),
    CallbackBasic {

    private val waitingDialog by lazy { WaitingDialog(this) }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else {
            super.onBackPressed()
        }
    }

    override fun chargingStarted() {
        showWait()
    }

    override fun successfulResult() {
        hideWait()
    }

    override fun failedQuery(reason: String?) {
        hideWait()
        val utils: Utils? = Utils(this)
        utils!!.setDialog("Cognitus",reason!!)
    }

    private fun showWait() {
        waitingDialog.showGeneric()
    }

    private fun hideWait() {
        waitingDialog.hideWDialog()
    }
}