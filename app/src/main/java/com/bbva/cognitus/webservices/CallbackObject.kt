package com.bbva.cognitus.webservices

interface CallbackObject<O> : CallbackBasic {
    fun obtainedValue(valor: O)
}