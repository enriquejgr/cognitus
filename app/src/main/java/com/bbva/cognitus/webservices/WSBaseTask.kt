package com.bbva.cognitus.webservices

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.bbva.cognitus.models.SimpleAnswer
import com.bbva.cognitus.utils.isValid
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.EOFException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.security.cert.CertPathValidatorException

abstract class WSBaseTask <S, C : CallbackBasic>(protected val context: Context,
                                                 protected val callback: C) :
    Callback<S>,
    LifecycleObserver {

    enum class ChargingPhases {
        INACTIVE, INITIAL, FINALIZED, FINALIZED_ERROR, FINALIZED_USER
    }

    private var parameters: List<String>? = null
    private var call: Call<S>? = null
    private var running: Boolean = false
    private var chargingPhases: ChargingPhases = ChargingPhases.INACTIVE

    private fun encodeB64(vararg params: String): String{
        this.parameters = listOf(*params)
        val elements = ArrayList<String>()
        elements.add(setIdService())
        elements.addAll(processParameters(*params))
        var oriString = TextUtils.join("|", elements)
        Log.i(javaClass.simpleName, "DatosParaB64->" + TextUtils.join("|", elements))
        val charset = Charsets.UTF_8
        return Base64.encodeToString(oriString.toByteArray(charset),Base64.DEFAULT)
    }

    protected fun startWS(vararg params: String) {
        Log.i(javaClass.simpleName, "Starting WS req")
        chargingPhases = ChargingPhases.INITIAL
        callback.chargingStarted()
        call = createCalled(encodeB64(*params))
        running = true
        call?.enqueue(this)
    }

    private fun processParameters(vararg params: String): List<String> {
        return listOf(*params)
    }

    protected abstract fun setIdService(): String

    protected abstract fun createCalled(encryptedParameters: String): Call<S>

    protected abstract fun validAnswer(response: S?): Boolean

    protected abstract fun processResponse(response: S?)

    private fun getErrorService(response: S?): String? {
        return if (response is SimpleAnswer)
            response.description
        else "Error al consultar, por favor intentelo de nuevo"
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun cancelRequest() {
        call?.let { if(running) it.cancel() }
    }

    override fun onResponse(call: Call<S>, response: Response<S>) {
        running = false
        if (response.isSuccessful) {
            if (validAnswer(response.body())) {
                Log.i(javaClass.simpleName, "Exitoso")
                chargingPhases = ChargingPhases.FINALIZED
                processResponse(response.body())
            } else
                showError(getErrorService(response.body()))
        } else
            showError("No se puede acceder al servicio")
    }

    override fun onFailure(call: Call<S>, t: Throwable) {
        running = false
        if (!call.isCanceled) {
            t.printStackTrace()
            if (networkAvailable()) {
                Log.e(javaClass.simpleName, "Error al consultar: " + call.request().url.toString())
                t.printStackTrace()
                when (t) {
                    is SocketTimeoutException, is ConnectException, is UnknownHostException,
                    is CertPathValidatorException ->
                        showError("No hay acceso en este momento, intenta de nuevo mas tarde")
                    is EOFException ->
                        showError("Ocurrió un error insesperado, intenta de nuevo mas tarde")
                    else ->
                        showError(getNotRecognizeError())
                }
            } else
                showError("Sin conexión a Internet")
        } else {
            chargingPhases = ChargingPhases.FINALIZED_USER
            Log.i(javaClass.simpleName, "Servicio cancelado manualmente")
        }
    }

    private fun networkAvailable(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val netActive = connectivityManager.activeNetwork ?: return false
            val networkCapabilities = connectivityManager.getNetworkCapabilities(netActive) ?: return false
            return when {
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else
            return connectivityManager.activeNetworkInfo?.isConnected ?: false
    }

    private fun showError(message: String?) {
        chargingPhases = ChargingPhases.FINALIZED_ERROR
        if(message != null && message.isValid()){
            Log.e(javaClass.simpleName, message)
            callback.failedQuery(message)
        } else {
            Log.e(javaClass.simpleName, "Error desconocido")
            callback.failedQuery(getNotRecognizeError())
        }
    }

    protected open fun getNotRecognizeError(): String =
        "No se puede realizar la acción. Intenta de nuevo."
}