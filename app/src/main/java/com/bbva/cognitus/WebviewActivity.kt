package com.bbva.cognitus

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity

class WebviewActivity : AppCompatActivity() {

	private lateinit var webView: WebView

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_webview)
		val urlImg:String = intent.getStringExtra("urlImg")
		webView = findViewById(R.id.webview)
		webView!!.settings.javaScriptEnabled = true
		webView.webViewClient = object : WebViewClient() {
			override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
				view?.loadUrl(url)
				return true
			}
		}
		webView.loadUrl(urlImg)
//        webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf);
	}
}