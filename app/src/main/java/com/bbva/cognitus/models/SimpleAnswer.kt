package com.bbva.cognitus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class SimpleAnswer (@SerializedName("err")@Expose var err: Boolean?,
                         @SerializedName("descripcion")@Expose var description: String?)