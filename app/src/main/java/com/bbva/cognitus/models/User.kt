package com.bbva.cognitus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User(@SerializedName("idusr") @Expose val usuId: String?,
           @SerializedName("usrnombre") @Expose val usuNombre: String?)
