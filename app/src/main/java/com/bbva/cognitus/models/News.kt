package com.bbva.cognitus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class News {
	@SerializedName("not_id") @Expose val notID: String?=null
	@SerializedName("not_img") @Expose val notImg: String?=null
	@SerializedName("not_url") @Expose val notUrl: String?=null
	@SerializedName("not_titulo") @Expose val notTitulo: String?=null
	@SerializedName("not_fecha") @Expose val notFecha: String?=null
	@SerializedName("not_desc") @Expose val notDesc: String?=null
}