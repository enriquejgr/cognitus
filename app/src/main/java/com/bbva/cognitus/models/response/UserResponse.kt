package com.bbva.cognitus.models.response

import com.bbva.cognitus.models.SimpleAnswer
import com.bbva.cognitus.models.User
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserResponse (
    err: Boolean,
    description: String,
    @SerializedName("valido")@Expose var validate:String?,
    @SerializedName("error")@Expose var error: String?,
    @SerializedName("usuario") @Expose var usuario: User?,
    @SerializedName("status")@Expose var status: String? ):
    SimpleAnswer(err, description)