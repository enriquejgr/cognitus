package com.bbva.cognitus.models.response

import com.bbva.cognitus.models.Notification
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NotificationResponse (@SerializedName("notificacion") @Expose val notificationsList: List<Notification>?)