package com.bbva.cognitus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Homework {
	@SerializedName("id_tarea") @Expose val id: String?=null
	@SerializedName("ta_area") @Expose val area: String?=null
	@SerializedName("ta_titulo") @Expose val title: String?=null
	@SerializedName("ta_desc") @Expose val description: String?=null
	@SerializedName("ta_tiempo") @Expose val time: String?=null
	@SerializedName("ta_nota") @Expose val note: String?=null
	@SerializedName("ta_tipo") @Expose val type: String?=null
	@SerializedName("ta_status") @Expose val status: String?=null
}