package com.bbva.cognitus.models.response

import com.bbva.cognitus.models.Homework
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HomeworkResponse (@SerializedName("tareas") @Expose val homeworkList: List<Homework>?)