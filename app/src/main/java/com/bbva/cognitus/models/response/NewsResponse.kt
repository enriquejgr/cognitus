package com.bbva.cognitus.models.response

import com.bbva.cognitus.models.News
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NewsResponse (@SerializedName("noticias") @Expose val newsList: List<News>?)