package com.bbva.cognitus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Notification {
	@SerializedName("not_id") @Expose val id: String?=null
	@SerializedName("not_titulo") @Expose val title: String?=null
	@SerializedName("not_desc") @Expose val description: String?=null
}