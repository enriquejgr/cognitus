package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.cognitus.databinding.ActivityRegisterBinding
import com.bbva.cognitus.utils.Utils

class RegisterActivity : ToolbarActivity(), Validator.ValidationListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegisterBinding>(this, R.layout.activity_register)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeToolbar(binding.toolBar, getString(R.string.activity_register_title), 0)

        val validator = Validator(binding).also {
            it.setValidationListener(this)
        }

        binding.setClickListener {
            when(it!!.id) {
                binding.btnRegister.id -> {
                    validator.toValidate()
                }
            }
        }

    }

    override fun onValidationSuccess() {
        if(binding.cbTerms.isChecked) {
//            TODO("Remover el toast e invocar webservice de registro")
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            val utils: Utils? = Utils(this)
            utils!!.setDialog(getString(R.string.app_name), getString(R.string.must_accept_terms))
        }
    }

    override fun onValidationError() {
        Toast.makeText(this@RegisterActivity, getString(R.string.wrong_data), Toast.LENGTH_SHORT).show()
    }
}