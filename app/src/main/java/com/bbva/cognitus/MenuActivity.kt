package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bbva.cognitus.databinding.ActivityMenuBinding

class MenuActivity: ToolbarMenuActivity() {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMenuBinding>(this, R.layout.activity_menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeToolbar(binding.toolBar)
        binding.setClickListener {
            when(it!!.id) {
                binding.menuNews.id -> {
                    startActivity(Intent(this, NewsListActivity::class.java))
//                    // Making crash the app on purpouse
//                    val crash: RegisterCrash? = RegisterCrash(this)
//                    crash!!.registerCrash("News", "Not available")
                }
                binding.menuCheck.id -> {
                    startActivity(Intent(this, CheckinActivity::class.java))
                }
                binding.menuNotifications.id -> {
                    startActivity(Intent(this, NotificationListActivity::class.java))
                }
                binding.menuProfile.id -> {
                    startActivity(Intent(this, ProfileActivity::class.java))
                }
                binding.menuTask.id -> {
                    startActivity(Intent(this, HomeworkActivity::class.java))
                }
            }
        }
    }
}