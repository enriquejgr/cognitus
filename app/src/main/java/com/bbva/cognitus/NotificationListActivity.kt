package com.bbva.cognitus

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bbva.cognitus.adapter.NotificationsAdapter
import com.bbva.cognitus.databinding.ActivityNotificationListBinding
import com.bbva.cognitus.models.Notification
import com.bbva.cognitus.task.NotificationsTask
import com.bbva.cognitus.utils.SharedPreference
import com.bbva.cognitus.utils.Utils
import com.bbva.cognitus.webservices.CallbackObjectList
import com.bbva.cognitus.webservices.WebServiceActivity
import com.google.gson.Gson
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class NotificationListActivity : WebServiceActivity(),
    CallbackObjectList<Notification>, View.OnClickListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityNotificationListBinding>(this, R.layout.activity_notification_list)
    }

    private val taskNotfications by lazy {
        NotificationsTask(this, this)
    }
    private var adapterNotification: RecyclerView.Adapter<*>? = null
    private lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeToolbar(binding.toolBar, getString(R.string.activity_notifications_title), 0)
        EventBus.getDefault().register(this)
        sharedPreference = SharedPreference(this)
        taskNotfications.startWS(sharedPreference.getValueString("userId")!!)
    }

    override fun showList(notificationsList : List<Notification>?) {
        if(notificationsList != null) {
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            binding.rvNotifications.layoutManager = layoutManager
            this.adapterNotification = NotificationsAdapter(notificationsList!!)
            binding.rvNotifications.adapter = adapterNotification
        }
    }

    override fun withoutResult() {
        val utils: Utils? = Utils(this)
        utils!!.setDialog(getString(R.string.app_name), getString(R.string.whitout_notifications))
    }

    override fun onClick(p0 : View?) {}

    @Subscribe
    fun NotificationReceived(event: com.bbva.cognitus.firebase.Notification) {
        val title = event.title
        val mensaje = event.message
        val datos = Gson().toJson(event.data)
        Toast.makeText(this, title, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }
}