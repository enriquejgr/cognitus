package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bbva.cognitus.databinding.ActivitySplashBinding
import com.bbva.cognitus.utils.SharedPreference

class SplashActivity: AppCompatActivity(), Runnable {
    private lateinit var binding: ActivitySplashBinding
    private lateinit var handler: Handler
    private lateinit var sharedPreference: SharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreference = SharedPreference(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        load()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus)
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    private fun load(){
        handler = Handler()
        handler.postDelayed(this, 2000)
    }

    override fun run() {
        if(sharedPreference.getValueBoolien("session", false)) {
            startActivity(Intent(this, MenuActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}