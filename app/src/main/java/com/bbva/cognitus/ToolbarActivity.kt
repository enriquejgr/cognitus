package com.bbva.cognitus

import android.app.ActionBar
import android.util.TypedValue
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bbva.cognitus.databinding.ToolbarBinding

abstract class ToolbarActivity : AppCompatActivity() {

  fun initializeToolbar(toolbarBinding: ToolbarBinding, title: String = "", tipoBtn: Int, subtitle: String = "") {
    setSupportActionBar(toolbarBinding.toolbar)
    toolbarBinding.toolbar.setNavigationOnClickListener {
  //            finish()
        onBackPressed()
    }
    toolbarBinding.title = title
    if(subtitle != "") {
      toolbarBinding.tvTituloB.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.toolbar_title_with_subtitle))
//      toolbarBinding.tvTituloB.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24F)
      toolbarBinding.subtitle = subtitle
      toolbarBinding.tvSubtitle.visibility = VISIBLE
      toolbarBinding.tvTituloB.layoutParams.height = WRAP_CONTENT
    }
    if(tipoBtn==1)
        toolbarBinding.ivBtn1.setImageResource(R.drawable.usuario_solid)

    toolbarBinding.setClickListener {
        when (it!!.id) {
            toolbarBinding.ivBtn1.id -> {
                Toast.makeText(this,"Botón barra ", Toast.LENGTH_LONG).show()
            }
        }
    }
  }
}