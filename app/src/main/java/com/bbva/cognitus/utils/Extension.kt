package com.bbva.cognitus.utils

import android.text.TextUtils
fun String?.isValid(): Boolean {
    return try {
        !TextUtils.isEmpty(this!!.trim { it <= ' ' })
    } catch (ignored: Exception) {
        false
    }
}