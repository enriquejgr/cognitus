package com.bbva.cognitus.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import androidx.appcompat.app.AlertDialog
import com.bbva.cognitus.databinding.DialogAlertBinding

object AlertDialogCustom {
	fun createAlert(context: Context,
									title: String?,
									message: String, isCancelInvisible: Boolean) {
		val builder = AlertDialog.Builder(context)

		val binding = DialogAlertBinding.inflate(LayoutInflater.from(context))
		binding.titulo = title
		binding.texto = message
		if(isCancelInvisible) {
			binding.btnCancel.visibility = GONE
		}

		builder.setView(binding.root)
		val d = builder.create()
		binding.btnAccept.setOnClickListener(View.OnClickListener {
			d.dismiss()
		})

		/*if (actionPositive.isValid()) {
				binding.accionPositiva = actionPositive
				binding.clickListenerPositivo = View.OnClickListener {
						listenerPositive?.inAction(OptionDialog.POSITIVE)
						d.dismiss()
				}
		}
		if (actionNegative.isValid()) {
				binding.accionNegativa = actionNegative
				binding.clickListenerNegativo = View.OnClickListener {
						listenerNegative?.inAction(OptionDialog.NEGATIVE)
						d.dismiss()
				}
		}*/
		d.setCancelable(false)
		d.setCanceledOnTouchOutside(true)
		d.show()
	}
}