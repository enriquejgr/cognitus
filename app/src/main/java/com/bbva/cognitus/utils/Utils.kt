package com.bbva.cognitus.utils

import android.content.Context
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide

class Utils (private val context: Context){
    fun setDialog(tittle:String,text:String): AlertDialog {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(tittle)
        builder.setMessage(text)
        builder.setPositiveButton("CERRAR"){dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
        return dialog
    }

    companion object {
        fun loadImage(contx: Context?, imv: ImageView, url: String, tipoImg: Int) {
            val requestManager = contx?.let { Glide.with(it) }
            var imageUri: String = ""
            imageUri = url
            val requestBuilder = requestManager?.load(imageUri)
            requestBuilder?.into(imv)
        }
    }
}