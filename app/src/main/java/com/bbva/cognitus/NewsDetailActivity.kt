package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bbva.cognitus.databinding.ActivityNewsDetailBinding
import com.bbva.cognitus.utils.Utils


class NewsDetailActivity : ToolbarActivity() {

	private val binding by lazy {
		DataBindingUtil.setContentView<ActivityNewsDetailBinding>(this, R.layout.activity_news_detail)
	}

	override fun onCreate(savedInstanceState : Bundle?) {
		super.onCreate(savedInstanceState)
		initializeToolbar(binding.toolBar, getString(R.string.activity_news_title), 0)

		val image: String? = intent.getStringExtra("image")
		val title: String? = intent.getStringExtra("title")
		val description: String? = intent.getStringExtra("description")
		val url: String? = intent.getStringExtra("url")

		binding.tvTitle.text = title
		binding.tvDescription.text = description
		Utils.loadImage(this, binding.ivImage, image.toString(), 1)

		binding.setClickListener {
			when(it!!.id) {
				binding.ivWatchMore.id -> {
					val intent = Intent(this, WebviewActivity::class.java)
					intent.putExtra("urlImg", url)
					startActivity(intent)
				}
				binding.ivShare.id -> {
					val intent = Intent(Intent.ACTION_SEND)
					intent.type = "text/plain"
					intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL")
					intent.putExtra(Intent.EXTRA_TEXT, url)
					startActivity(Intent.createChooser(intent, "Share URL"))
				}
			}
		}
	}
}