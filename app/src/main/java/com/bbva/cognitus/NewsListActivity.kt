package com.bbva.cognitus

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bbva.cognitus.adapter.NewsAdapter
import com.bbva.cognitus.databinding.ActivityNewsListBinding
import com.bbva.cognitus.models.News
import com.bbva.cognitus.task.NewsTask
import com.bbva.cognitus.webservices.CallbackObjectList
import com.bbva.cognitus.webservices.WebServiceActivity

class NewsListActivity : WebServiceActivity(),
    CallbackObjectList<News>,
    View.OnClickListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityNewsListBinding>(this, R.layout.activity_news_list)
    }

    private val taskNews by lazy { NewsTask(this, this) }
    private var adapterNews: RecyclerView.Adapter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeToolbar(binding.toolBar, getString(R.string.activity_news_title), 0)
        taskNews.startWS()
    }

    override fun showList(newsList: List<News>?) {
        if (newsList != null) {
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
            binding.rvNoticias.layoutManager = layoutManager
            this.adapterNews = NewsAdapter(newsList!!)
            binding.rvNoticias.adapter = adapterNews
            for (news in newsList)
                Log.i("CallbackNewa", "titulo: ${news.notTitulo}")
        }
    }

    override fun withoutResult() {
        Log.w("CallbackNoticia", "Sin resultados")
    }

    override fun onClick(v: View?) {}
}