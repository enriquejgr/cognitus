package com.bbva.cognitus

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.location.LocationListener
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bbva.cognitus.databinding.ActivityCheckinBinding
import com.github.gcacace.signaturepad.views.SignaturePad
import kotlinx.android.synthetic.main.activity_checkin.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class CheckinActivity: ToolbarActivity(), Validator.ValidationListener {


    /**
     * Signature
     */
    companion object {
        private const val STORAGE_REQUEST = 200
        private const val FILE_NAME = "Signature.png"
        private const val DIRECTORY_NAME = "/checker"
    }

    /**
     * Locations
     */
    private var locationManager : LocationManager? = null
    private val TAG = "CheckIn Activity"
    private val LOCATION_REQUEST=1500
    private var latitude = 0.0
    private var longitude = 0.0

    /**
     * Validator
     */
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityCheckinBinding>(this, R.layout.activity_checkin)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /**
         * Toolbar
         */
        initializeToolbar(binding.toolBar, getString(R.string.activity_checkin_title), 0)

        val validator = Validator(binding).also {
            it.setValidationListener(this)
        }

        /**
         * Setting click listeners
         */
        binding.setClickListener {
            when(it!!.id) {
                binding.confirmButton.id -> {
                    validator.toValidate()
                }
            }
        }

        /**
         * Location
         */
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        checkLocationPermission()

        /**
         * Signature
         */
        signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {}
            override fun onClear() {}
            override fun onSigned() {
                confirmButton.isEnabled = true
            }
        })
    }

    /**
     * Validator
     */
    override fun onValidationSuccess() {
        checkStoragePermission()
    }

    private fun saveSignature() {
        val signatureBitmap: Bitmap = signaturePad.getTransparentSignatureBitmap()
        if (addJpgSignatureToGallery(signatureBitmap)) {
            //TODO("Hacer un llamado a servicio incluyendo hora, imagen y posición geográfica")
            startActivity(Intent(this, MenuActivity::class.java))
        } else {
            Toast.makeText(this, getString(R.string.not_saved_signature), Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Validator
     */
    override fun onValidationError() {
        Toast.makeText(this@CheckinActivity, getString(R.string.wrong_data), Toast.LENGTH_SHORT).show()
    }

    /**
     * Location
     */
    private fun checkLocationPermission() =
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST)
        } else {
            getLocation()
        }

    private fun checkStoragePermission(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_REQUEST)
        } else {
            saveSignature()
        }
    }

    /**
     * Location
     */
    private fun getLocation(){
        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
        } catch(ex: SecurityException) {
            Log.d(TAG, "Security Exception, no location available")
        }
    }

    /**
     * Location
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQUEST -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation()
            } else {
                Toast.makeText(this, getString(R.string.permission_required), Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, MenuActivity::class.java))
            }
            STORAGE_REQUEST -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                saveSignature()
            } else {
                Toast.makeText(this, getString(R.string.permission_required), Toast.LENGTH_SHORT).show()
            }
        }
    }

    /**
     * Location
     */
    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            longitude = location.longitude
            latitude = location.latitude
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    /**
     * Signature
     */
    private fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
        var result = false
        try {
            val path = Environment.getExternalStorageDirectory().toString() + DIRECTORY_NAME
            Log.d("Files", "Path: $path")
            val fileFirm = File(path)
            fileFirm.mkdirs()
            val photo = File(fileFirm, FILE_NAME)
            Log.d("Files", "Path: $photo")
            saveBitmapToPNG(signature, photo)
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    /**
     * Signature
     */
    @Throws(IOException::class)
    fun saveBitmapToPNG(bitmap: Bitmap, photo: File) {
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(photo)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                out?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}