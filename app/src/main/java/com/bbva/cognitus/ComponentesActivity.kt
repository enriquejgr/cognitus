package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bbva.cognitus.databinding.ActivityComponentesBinding
import com.bbva.cognitus.models.User
import com.bbva.cognitus.task.LoginTask
import com.bbva.cognitus.utils.AlertDialogCustom
import com.bbva.cognitus.webservices.CallbackObject
import com.bbva.cognitus.webservices.WebServiceActivity
import kotlinx.android.synthetic.main.activity_componentes.*

class ComponentesActivity : WebServiceActivity(),
	CallbackObject<User>, View.OnClickListener {
	private val binding by lazy {
		DataBindingUtil.setContentView<ActivityComponentesBinding>(this, R.layout.activity_componentes)
	}

	private val taskLogin by lazy { LoginTask(this, this) }

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding.clickListener = this
		taskLogin.startWS("luis@gmail.com", "123456")
	}

	override fun obtainedValue(valor: User) {
		if (valor != null) {
			Log.i("Valor->", valor.usuNombre)
			binding.usrName = valor.usuNombre
		}
	}

	override fun onClick(v: View?) {
		when (v?.id) {
			R.id.btnNews -> {
				startActivity(
					Intent(this, NewsListActivity::class.java)
				)
			}
			R.id.showHideBtn->{
				if(binding.showHideBtn.text.toString().equals("Show")){
					pwd.transformationMethod = HideReturnsTransformationMethod.getInstance()
					showHideBtn.text = "Hide"
				} else{
					pwd.transformationMethod = PasswordTransformationMethod.getInstance()
					showHideBtn.text = "Show"
				}
			}
			R.id.btnDAlert->{

				AlertDialogCustom.createAlert(this,"Titulo de prueba","Hola es un mensaje", true)
			}
		}
	}


}