package com.bbva.cognitus

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bbva.cognitus.databinding.ActivityMainBinding
import com.bbva.cognitus.models.User
import com.bbva.cognitus.task.LoginTask
import com.bbva.cognitus.utils.SharedPreference
import com.bbva.cognitus.webservices.CallbackObject
import com.bbva.cognitus.webservices.WebServiceActivity

class MainActivity : WebServiceActivity(), View.OnClickListener, CallbackObject<User> {

    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPreference: SharedPreference

    private val taskLogin by lazy { LoginTask(this,this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreference = SharedPreference(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.clickListener = this
//        EventBus.getDefault().register(this)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.hide()
            actionBar.setDisplayHomeAsUpEnabled(false)
            actionBar.setDisplayShowHomeEnabled(false)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnStart -> {
                taskLogin.startWS(binding.etUser.text.toString(), binding.etPassword.text.toString())
            }
            R.id.tvRegister -> {
                sharedPreference.clearSharedPreference()
                startActivity(Intent(this, RegisterActivity::class.java))
            }
            R.id.tvForgottenPassword -> {
                startActivity(Intent(this, ForgottenPasswordActivity::class.java))
            }
        }
    }

    override fun obtainedValue(valor: User) {
        if(valor!=null) {
            sharedPreference.save("session", true)
            sharedPreference.save("userName", valor.usuNombre!!)
            sharedPreference.save("userId", valor.usuId!!)
            startActivity(Intent(this, MenuActivity::class.java))
            finish()
        }
    }

    /*@Subscribe
    fun NotificationReceived(event: Notification) {
        val title = event.title
        val mensaje = event.message
        val datos = Gson().toJson(event.data)
        Toast.makeText(this, title, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }*/
}